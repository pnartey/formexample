package com.androidclass.form;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by philip on 6/3/17.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText firstName;
    EditText lastName;
    RadioButton radioMale, radioFemale;
    CheckBox checkBelow18, checkAbove18;
    Button btnSubmit;
    TextView textName, textGender, textStatus;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        firstName= (EditText) findViewById(R.id.first_name);
        lastName= (EditText) findViewById(R.id.last_name);
        radioMale= (RadioButton) findViewById(R.id.radio_male);
        radioFemale= (RadioButton) findViewById(R.id.radio_female);
        checkAbove18= (CheckBox) findViewById(R.id.check_above18);
        checkBelow18= (CheckBox) findViewById(R.id.check_below18);

        btnSubmit= (Button) findViewById(R.id.btn_submit);

        textName= (TextView) findViewById(R.id.text_name);
        textGender= (TextView) findViewById(R.id.text_gender);
        textStatus= (TextView) findViewById(R.id.text_status);

        checkAbove18.setOnClickListener(this);
        checkBelow18.setOnClickListener(this);
    }


    public void submit(View v){

        if(this.firstName.getText().toString().isEmpty()){
            Toast.makeText(this, getString(R.string.please_enter_first_name), Toast.LENGTH_LONG).show();
            return;
        }

        if(this.lastName.getText().toString().isEmpty()){
            Toast.makeText(this, getString(R.string.please_enter_last_name), Toast.LENGTH_LONG).show();
            return;
        }

        if(!radioMale.isChecked() && !radioFemale.isChecked()){
            Toast.makeText(this, getString(R.string.please_select_gender), Toast.LENGTH_LONG).show();
            return;
        }

        if(!checkAbove18.isChecked() && !checkAbove18.isChecked()){
            Toast.makeText(this, getString(R.string.please_select_age_range), Toast.LENGTH_LONG).show();
            return;
        }

        String firstName= this.firstName.getText().toString();
        String lastName= this.lastName.getText().toString();
        textName.setText(firstName+" "+lastName);

        if(radioMale.isChecked()){
            textGender.setText(getString(R.string.male));
        }

        if(radioFemale.isChecked()){
            textGender.setText(getString(R.string.female));
        }

        if(checkAbove18.isChecked()){
            textStatus.setText(getString(R.string.age_above_18));
        }

        if(checkBelow18.isChecked()){
            textStatus.setText(getString(R.string.age_below_18));
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.check_below18:
                checkAbove18.setChecked(false);
                break;
            case R.id.check_above18:
                checkBelow18.setChecked(false);
                break;
        }
    }
}
